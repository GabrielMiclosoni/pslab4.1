#ifndef MENUITEM_H
#define MENUITEM_H
#include "projectlibs.h"


class MenuItem
{
    private:
        string name;
    public:
        MenuItem(string name): name(name) {}
        virtual bool do_stuff() = 0;
        string get_name(){ return name; }
};


#endif // MENUITEM_H

